import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './_services';
import { User } from './_models';
import { logonSaiService } from './services/logon.sai.service';
import { LogInResponse } from '../app/_classes/loginresponse';
import { HttpClient } from '@angular/common/http';


@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent {
    userName: string = '';
    currentUser: User;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private _logonSaiService: logonSaiService,
        private httpClient:HttpClient
        
    ) {
        
    }

    logInResponse: LogInResponse;  

    ngOnInit() {

    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }

}