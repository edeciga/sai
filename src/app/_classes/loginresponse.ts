
export class LogInResponse
{
    User: string;
    Password: string;
    success: boolean;
    token: string;
    message: string;
}
